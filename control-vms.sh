#!/bin/bash
#//***********************************************************//#
#  controlvms.sh
#  @name: Bash VirtualBox Script
#  @author: Henry Bravo
#  @version: 0.2
#  @date: 21.07.2014
#  @desc: minimal menu based management for VirtualBox VM's
#         for the most repetitive tasks
#//***********************************************************//#

trap '' SIGINT SIGQUIT SIGTSTP
STD='\033[0;0;39m'
RED='\033[0;41;30m'
BLUE='\033[0;34m'
YELLOW='\033[1;33m'
NO_COLOUR='\033[0m'
LIGHT_GRAY='\033[0;37m'
LIGHT_GREEN='\033[1;32m'
LIGHT_BLUE='\033[1;34m'
LIGHT_CYAN='\033[1;36m'

# check if vboxmanage exists
if [ ! $(which vboxmanage) ] ; then clear
  echo -e "${RED}cannot find vboxmanage! ...exiting now...${STD}"
  exit 1
fi

header() {
  clear
  echo -e "${YELLOW}+-------------------------------------------------+"
  echo -e "|            Bash VirtualBox Script               |"
  echo -e "+-------------------------------------------------+${STD}"
}

showvms(){
  echo -e "${LIGHT_GREEN}+installed vms${STD}"
  vmlist
}

showrunning() {
  running=$(vboxmanage list runningvms)
  echo -e "${LIGHT_GREEN}+running vms${STD}"
  if [ -n "$running" ] ; then
    echo -e "${LIGHT_GRAY}$running${STD}"
    running=TRUE
  else
    echo -e "${LIGHT_GRAY}no running vms${STD}"
    running=FALSE
  fi
}

vmlist(){
  VMSLIST=$(vboxmanage list vms | awk -F" " '{print $1}' | tr -d '"')
  vmsl=($VMSLIST)
  for i in ${vmsl[@]} ; do
    echo $i
  done
}

snapshotlist(){
  #SSLIST=$(vboxmanage showvminfo $VM | grep "Name:" | sed -n '1!p' | awk -F" " '{print $2}')
  slist=$(vboxmanage showvminfo $VM | grep "Name:" | sed -n '1!p' | awk -F" " '{print $2}')
  for i in ${slist[@]} ; do
  	echo $1
  done
}

natnetslist(){
  clear 
  header
  echo -e "${LIGHT_GREEN}+configured nat networks${STD}"
  vboxmanage list natnets | grep -v IPv6 | grep -v loopback
  read -p "Press ENTER to continue..."
}

selectvm(){
  echo -e "${LIGHT_BLUE}+-------------------------------------------------+${STD}"
  echo -e "${LIGHT_BLUE}| select a vm                                     |${STD}"
  echo -e "${LIGHT_BLUE}+-------------------------------------------------+${STD}"
  select vms in $(vmlist) ; do
    VM=$vms
    break
  done
}

selectsnapshot() {
  echo -e "${LIGHT_BLUE}+-----------------------------------------------------+${STD}"
  echo -e "${LIGHT_BLUE}| select a snapshot                                   |${STD}"
  echo -e "${LIGHT_BLUE}|                                                     |${STD}"
  echo -e "${LIGHT_BLUE}| the currect snapshot is always the last in the list |${STD}"
  echo -e "${LIGHT_BLUE}+-----------------------------------------------------+${STD}"
  select snapshot in $(snapshotlist) ; do
    SNAPSHOT=$snapshot
    break
  done
}

startvm() {
  vboxmanage startvm $VM --type headless
}

savevm() {
  vboxmanage controlvm $VM savestate
}

infovm() {
  echo -e "${LIGHT_GREEN}# $vm${STD}"
  vboxmanage guestproperty enumerate $VM | awk '{print $2,$3,$4}' | sed 's/\/VirtualBox\///' | more
  echo ''
  read -p "Press ENTER to continue..."
}

restoresnapshot(){
  # restore the last taken snapshot
  # see http://www.virtualbox.org/manual/ch08.html#idp58964576
  vboxmanage snapshot $SNAPSHOT $VM restore
}

menu() {
  echo -e "${YELLOW}+-------------------------------------------------+"
  echo -e "| 1. start vm                                     |"
  echo -e "| 2. savestate vm                                 |"
  echo -e "| 3. restore vm                                   |"
  echo -e "| 4. info vm                                      |"
  echo -e "| 5. show nat networks                            |"
  echo -e "| 6. exit                                         |"
  echo -e "+-------------------------------------------------+${STD}";
  local choice
    read -p "Enter choice [ 1 - 6]" choice
  case $choice in
    1) selectvm ; startvm ;;
    2) if [ $running = FALSE ] ; then
            echo -e "${RED}no running vms to savestate${STD}" && sleep 1
       else
            selectvm ; savevm
       fi
       ;;
    3) selectvm ; selectsnapshot ;; #restoresnapshot
    4) selectvm ; infovm ;;
    5) natnetslist ;;
    6) clear ; exit 0 ;;
    *) echo -e "${RED}Error...${STD}" && sleep 1 ;;
  esac
}


main(){
  while true
    do
      header
      showvms
      echo ''
      showrunning
      echo ''
      menu
  done
}

# run programs
main
