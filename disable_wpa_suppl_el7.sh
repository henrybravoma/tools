#!/bin/bash

# This script disables the "wpa_supplicant" service 
# as normal disabling procedure does not work
# EDIT: this bug seems to be solved as of release 7.3 of RHEL & CentOS

# patch the d-bus service
cp /usr/share/dbus-1/system-services/fi.w1.wpa_supplicant1.service /usr/share/dbus-1/system-services/fi.w1.wpa_supplicant1.service.bck
cat > /usr/share/dbus-1/system-services/fi.w1.wpa_supplicant1.service <<WPA_SUP
[D-BUS Service]
Name=fi.w1.wpa_supplicant1
Exec=/usr/sbin/wpa_supplicant -B -u -f /var/log/wpa_supplicant.log -c /etc/wpa_supplicant/wpa_supplicant.conf -P /var/run/wpa_supplicant.pid
User=root
SystemdService=dbus-wpa_supplicant.service
WPA_SUP

# patch the wpa_supplicant.service
cp /usr/lib/systemd/system/wpa_supplicant.service /usr/lib/systemd/system/wpa_supplicant.service.bck
cat > /usr/lib/systemd/system/v <<'WPA_SER'
[Unit]
Description=WPA Supplicant daemon
Before=network.target
After=syslog.target

[Service]
Type=dbus
BusName=fi.w1.wpa_supplicant1
EnvironmentFile=-/etc/sysconfig/wpa_supplicant
ExecStart=/usr/sbin/wpa_supplicant -u -f /var/log/wpa_supplicant.log -c /etc/wpa_supplicant/wpa_supplicant.conf $INTERFACES $DRIVERS $OTHER_ARGS

[Install]
WantedBy=multi-user.target
Alias=dbus-wpa_supplicant.service
WPA_SER

# reload daemon so no reboot is required
/bin/systemctl daemon-reload

echo -e "\n==> service files have patched, either reboot or stop the wpa_supplicant service\n"
