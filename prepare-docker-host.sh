#!/bin/bash
#
#//**************************************************//
# @name:   prepare-docker-host.sh
# @author: Henry Bravo
# @date:   March 3 2017
# @desc:   prepares RHEL7 hosts to become Docker
#          hosts installing docker-ce
#//*************************************************//

# How to use:
#
# Toggle functions called from the main section
# at the end of this script. First the kernel parameters
# must be correct and the host rebooted after. Then the
# rest of the script can be executed.
#
# start with setting the DOCKER_DISK in 39
#
# current functions are:
# 1. precheck
# 2. setkernel
# 3. addyumrepos => TO BE COMPLETED
# 4. installdocker => TO BE COMPLETED
# 5. setupstorage
# 6. adddockergroup
# 7. configureproxy
# 8. enablestartdocker

# add some colours
STD='\033[0;0;39m'
RED='\033[0;41;30m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
NO_COLOUR='\033[0m'

# Set the docker disk for the thinpool
# e.g. DOCKER_DISK='/dev/sdb'
DOCKER_DISK='/dev/vdb'

# pre checks
precheck() {
  KERNELSETTINGS=$(grep 'GRUB_CMDLINE_LINUX' /etc/default/grub)
  if [ ! -e "$KERNELSETTINGS" ] ; then
    echo
    echo -e "${GREEN}==> current kernel default settings:${NO_COLOUR}"
    echo KERNELSETTINGS: "$KERNELSETTINGS"
    echo
  fi

  YUMREPOS=$(grep rhel-7 /etc/yum.repos.d/*.repo)
  if [ ! -e "$YUMREPOS" ] ; then
    echo
    echo -e "${GREEN}==> current yum repo for RHEL7:${NO_COLOUR}"
    echo YUMREPOS: "$YUMREPOS"
    echo
  fi

  DOCKERINSTALLED=$(rpm -qa | grep docker)
  if [ ! -e "$DOCKERINSTALLED" ] ; then
    echo
    echo -e "${GREEN}==> docker packages found:${NO_COLOUR}"
    echo DOCKERINSTALLED: "$DOCKERINSTALLED"
    echo
  fi

  HASSTORAGE=$(cat /etc/lvm/profile/docker-thinpool0.profile)
  if [ ! -e "$HASSTORAGE" ] ; then
    echo
    echo -e "${GREEN}==> docker storage found:${NO_COLOUR}"
    echo HASSTORAGE: "$HASSTORAGE"
    echo
  fi

  DOCKERGROUP=$(/bin/getent group docker)
  DOCKERGID=$(/bin/getent group 700)
  if [ "$DOCKERGROUP" ] && [ "$DOCKERGID" ] ; then
    echo
    echo -e "${GREEN}==> adddockergroup: value(s) exists${NO_COLOUR}"
    echo DOCKERGROUP: "$DOCKERGROUP"
    echo DOCKERGID: "$DOCKERGID"
    echo
  fi
  
  HASPROXY=$()
  if [ -ne "$HASPROXY" ] ; then
    echo
    echo -e "${GREEN}==> docker proxy found:${NO_COLOUR}"
    echo HASPROXY: "$HASPROXY"
    echo
  fi

  DOCKERSERVICE=$(systemctl status docker.service | head -n 10)
  if [ "$DOCKERSERVICE" ] ; then
    echo
    echo -e "${GREEN}==> docker is running:${NO_COLOUR}"
    echo DOCKERSERVICE: "$DOCKERSERVICE"
    echo
  fi
}

setkernel() {
  # kernel startup
  GRUB="/etc/default/grub"
(
cat <<EOF
GRUB_TIMEOUT=3
GRUB_DISTRIBUTOR="Red Hat Enterprise Linux Server"
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1 rd.lvm.lv=vg_root/lv_root rd.lvm.lv=vg_root/lv_usr rd.lvm.lv=vg_root/lv_swap crashkernel=auto biosdevname=0 net.ifnames=0 ipv6.disable=1"
GRUB_DISABLE_RECOVERY="true"
EOF
) > ${GRUB}
  grub2-mkconfig -o /boot/grub2/grub.cfg

  # make sure firewalld is turned off
  systemctl disable firewalld.service

  # reboot the system
  echo
  echo "==> Please reboot the system in order to process the changes and proceed"
  echo
  sleep 3
  exit 0
  # reboot
}

addrepo(){
  # docker-ce repo
  REPO="/etc/yum.repos.d/docker-ce.repo"
(
cat << EOF
[docker-ce-stable]
name=Docker CE Stable - $basearch
baseurl=https://download.docker.com/linux/centos/7/$basearch/stable
enabled=1
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg

[docker-ce-stable-debuginfo]
name=Docker CE Stable - Debuginfo $basearch
baseurl=https://download.docker.com/linux/centos/7/debug-$basearch/stable
enabled=0
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg

[docker-ce-stable-source]
name=Docker CE Stable - Sources
baseurl=https://download.docker.com/linux/centos/7/source/stable
enabled=0
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg

[docker-ce-edge]
name=Docker CE Edge - $basearch
baseurl=https://download.docker.com/linux/centos/7/$basearch/edge
enabled=0
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg

[docker-ce-edge-debuginfo]
name=Docker CE Edge - Debuginfo $basearch
baseurl=https://download.docker.com/linux/centos/7/debug-$basearch/edge
enabled=0
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg

[docker-ce-edge-source]
name=Docker CE Edge - Sources
baseurl=https://download.docker.com/linux/centos/7/source/edge
enabled=0
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg
EOF
) > ${REPO}
}

installdocker() {
  # install docker
  yum update -y
  yum install -y --enablerepo=docker-ce-stable docker
  yum install -y wget git net-tools bind-utils iptables-services bridge-utils bash-completion
}

setupstorage() {
# prepare the storage
  STORG="/etc/sysconfig/docker-storage"
(
cat << 'EOF'
DOCKER_STORAGE_OPTIONS="--storage-driver devicemapper --storage-opt dm.fs=xfs --storage-opt dm.thinpooldev=/dev/mapper/vg_docker-thinpool0 --storage-opt dm.use_deferred_removal=true --storage-opt dm.use_deferred_deletion=true"
EOF
) > ${STORG}

  STORAGEDAEMON="/etc/docker/daemon.json"
(
cat << 'EOF'
{
     "storage-driver": "devicemapper",
     "storage-opts": [
         "dm.thinpooldev=/dev/mapper/vg_docker-thinpool0",
         "dm.use_deferred_removal=true"
     ]
 }
EOF
) > ${STORAGEDAEMON}

  THINPOOLPROFILE="/etc/lvm/profile/docker-thinpool0.profile"
(
cat << 'EOF'
activation {
    thin_pool_autoextend_threshold=80
    thin_pool_autoextend_percent=20
}
EOF
) > ${THINPOOLPROFILE}

  # create the thinpool
  if [[ ! "$DOCKER_DISK" ]] ; then
    echo
    echo "==> No disk was provided, please define $DOCKER_DISK"
    echo
    exit 1
  else
    pvcreate /dev/sdb
    vgcreate vg_docker /dev/sdb
    lvcreate --wipesignatures y -n thinpool0 vg_docker -l 95%VG
    lvcreate --wipesignatures y -n thinpoolmeta0 vg_docker -l 1%VG
    lvconvert -y --zero n -c 512K --thinpool vg_docker/thinpool0 --poolmetadata vg_docker/thinpoolmeta0
    lvchange --metadataprofile docker-thinpool0 vg_docker/thinpool0
    lvs -o+seg_monitor
  fi
  
  # check whether there are remnants of the docker-storage-setup
  systemctl disable docker-storage-setup.service
  systemctl stop docker-storage-setup.service
  
  sleep 3
}

adddockergroup() {
  # create the Docker group if not present
  DOCKERGROUP=$(/bin/getent group docker)
  DOCKERGID=$(/bin/getent group 700)
  if [ ! "$DOCKERGROUP" ] && [ ! "$DOCKERGID" ] ; then
    echo adding group docker
    groupadd -g 700 docker
  else
    echo
    echo "==> adddockergroup: value(s) exists"
    echo DOCKERGROUP: "$DOCKERGROUP"
    echo DOCKERGID: "$DOCKERGID"
    echo
  fi
}

configureproxy() {
  # create docker config directory
  DCKRCONFIG="/etc/systemd/system/docker.service.d"
  if [ ! -d "$DCKRCONFIG" ] ; then
    mkdir $DCKRCONFIG
  fi
  # add config file
  PROXYCONF="/etc/systemd/system/docker.service.d/http-proxy.conf"
  if [ -e "$PROXYCONF" ] ; then
    cp ${PROXYCONF} ${PROXYCONF}.OLD
  fi
(
cat << 'EOF'
[Service]
Environment="HTTP_PROXY=http://proxy-server" "NO_PROXY=localhost,127.0.0.1,ao-srv.com"
EOF
) > ${PROXYCONF}
  systemctl daemon-reload
  # check if it's correct
  echo
  echo "==> the proxy is correctly added"
  systemctl show --property=Environment docker
  echo
}

enablestartdocker() {
  systemctl enable docker
  systemctl start docker
  # docker info
}

echo -e "${GREEN}#//**************************************************//"
echo -e "${GREEN}@name:   prepare-docker-host.sh"
echo -e "${GREEN}@author: Henry Bravo"
echo -e "${GREEN}@date:   March 3 2017"
echo -e "${GREEN}@desc:   prepares RHEL7 hosts to become Docker"
echo -e "${GREEN}         hosts installing docker-ce"
echo -e "${GREEN}#//*************************************************//"
echo -e "${NO_COLOUR}"
hostnamectl | grep -i hostname
echo -e "${GREEN}#//*************************************************//${NO_COLOUR}"

#######################################################
# main                                                #
# comment out what you don't need or already executed #
# first step is setkernel and reboot then the rest    #
#######################################################
precheck
#setkernel
#
#addrepo
#installdocker
#setupstorage
#adddockergroup
#configureproxy
#enablestartdocker
